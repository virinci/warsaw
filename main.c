#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef double Num;

typedef enum TokenType {
	TT_NUMBER,

	TT_PLUS,
	TT_HYPHEN,
	TT_ASTERISK,
	TT_SLASH,
	TT_CARET,

	TT_PARENTHESIS_LEFT,
	TT_PARENTHESIS_RIGHT,

	TT_EOF,
} TokenType;

static const char *token_type_debug[] = {
	"TT_NUMBER",

	"TT_PLUS",
	"TT_HYPHEN",
	"TT_ASTERISK",
	"TT_SLASH",
	"TT_CARET",

	"TT_PARENTHESIS_LEFT",
	"TT_PARENTHESIS_RIGHT",

	"TT_EOF",
};

typedef struct TokenNumber {
	Num num;
} TokenNumber;

typedef struct Token {
	TokenType type;

	union {
		TokenNumber number;
	} literal;
} Token;

#define GENERIC_STACK_ALIAS StackToken
#define GENERIC_STACK_T Token
#define GENERIC_STACK_METHOD(fn) stack_token_##fn
#define GENERIC_STACK_IMPLEMENTATION
#define GENERIC_STACK_STATIC
#include "generic_stack.h"

#define GENERIC_STACK_ALIAS StackNum
#define GENERIC_STACK_T Num
#define GENERIC_STACK_METHOD(fn) stack_num_##fn
#define GENERIC_STACK_IMPLEMENTATION
#include "generic_stack.h"

Token token_new(TokenType type) {
	return (Token){.type = type};
}

void token_log(Token *self) {
	if (self->type == TT_NUMBER) {
		printf("Token { type: %s\tliteral: %lf }\n", token_type_debug[self->type], self->literal.number.num);
	} else {
		printf("Token { type: %s }\n", token_type_debug[self->type]);
	}
}

Token *lexer_analyze(const char *str) {
	int x = 0;
	int len = (int) strlen(str);

	Token *tokens = (Token *) malloc(1024 * sizeof(Token));
	int token_index = 0;

	while (x < len) {
		char c = str[x];

		if (c == ' ' || c == '\r' || c == '\t' || c == '\n') {
			x += 1;
			continue;
		}

		switch (c) {
		case '+':
			tokens[token_index] = token_new(TT_PLUS);
			x += 1;
			break;
		case '-':
			tokens[token_index] = token_new(TT_HYPHEN);
			x += 1;
			break;
		case '*':
			tokens[token_index] = token_new(TT_ASTERISK);
			x += 1;
			break;
		case '/':
			tokens[token_index] = token_new(TT_SLASH);
			x += 1;
			break;
		case '^':
			tokens[token_index] = token_new(TT_CARET);
			x += 1;
			break;
		case '(':
			tokens[token_index] = token_new(TT_PARENTHESIS_LEFT);
			x += 1;
			break;
		case ')':
			tokens[token_index] = token_new(TT_PARENTHESIS_RIGHT);
			x += 1;
			break;
		default: {
			char *leftover = NULL;
			const Num num = strtod(str + x, &leftover);
			assert(leftover != NULL);

			if (leftover == str + x) {
				exit(1);
			}

			x = (int) (leftover - str);
			// printf("num: %lf\t%p\t%p\n", num, str, leftover);

			tokens[token_index] = (Token){.type = TT_NUMBER, .literal = {.number = {.num = num}}};
		}
		}

		token_index += 1;
		// printf("%s\n", str);
		// printf("token_index: %d\tx: %d\tc: `%c`\n", token_index, x, c);
	}

	tokens[token_index] = token_new(TT_EOF);
	return tokens;
}

int operator_precedence(TokenType type) {
	int precedence = -1;

	switch (type) {
	case TT_CARET:
		precedence = 100;
		break;
	case TT_ASTERISK:
	case TT_SLASH:
		precedence = 90;
		break;
	case TT_PLUS:
	case TT_HYPHEN:
		precedence = 50;
		break;
	default:
		precedence = -1;
	}

	if (precedence < 0) {
		printf("%s\n", token_type_debug[type]);
	}

	assert(precedence != -1 && "Wrong token type provided for calculating precedence");
	return precedence;
}

typedef enum {
	LEFT_TO_RIGHT,
	RIGHT_TO_LEFT,
} Associativity;

Associativity operator_associativity(TokenType type) {
	Associativity assoc = LEFT_TO_RIGHT;

	switch (type) {
	case TT_CARET:
		assoc = RIGHT_TO_LEFT;
		break;
	default:
	case TT_ASTERISK:
	case TT_SLASH:
	case TT_PLUS:
	case TT_HYPHEN:
		assoc = LEFT_TO_RIGHT;
		break;
	}

	return assoc;
}

void token_stack_log(StackToken *self) {
	printf("======\n");
	for (size_t x = 0; x < stack_token_len(self); ++x) {
		token_log(&self->buf[x]);
	}
	printf("------\n");
}

Num evaluate_postfix(StackToken *postfix) {
	StackToken reverse_postfix[1];
	stack_token_new(reverse_postfix, stack_token_capacity(postfix));

	while (!stack_token_is_empty(postfix)) {
		stack_token_push(reverse_postfix, stack_token_pop(postfix));
	}

	StackNum accumulator[1];
	stack_num_new(accumulator, stack_token_capacity(postfix));

	while (!stack_token_is_empty(reverse_postfix)) {
		Token token = stack_token_pop(reverse_postfix);

		if (token.type == TT_NUMBER) {
			stack_num_push(accumulator, token.literal.number.num);
			continue;
		}

		switch (token.type) {
		case TT_PLUS: {
			Num rhs = stack_num_pop(accumulator);
			Num lhs = stack_num_pop(accumulator);
			stack_num_push(accumulator, lhs + rhs);
		} break;
		case TT_HYPHEN: {
			Num rhs = stack_num_pop(accumulator);
			Num lhs = stack_num_pop(accumulator);
			stack_num_push(accumulator, lhs - rhs);
		} break;
		case TT_ASTERISK: {
			Num rhs = stack_num_pop(accumulator);
			Num lhs = stack_num_pop(accumulator);
			stack_num_push(accumulator, lhs * rhs);
		} break;
		case TT_SLASH: {
			Num rhs = stack_num_pop(accumulator);
			Num lhs = stack_num_pop(accumulator);
			stack_num_push(accumulator, lhs / rhs);
		} break;
		case TT_CARET: {
			Num rhs = stack_num_pop(accumulator);
			Num lhs = stack_num_pop(accumulator);
			stack_num_push(accumulator, pow(lhs, rhs));
		} break;
		default:
			assert(0);
		}
	}

	const Num value = stack_num_pop(accumulator);

	stack_token_free(reverse_postfix);
	stack_num_free(accumulator);
	return value;
}

void stack_token_reverse(StackToken *stack) {
	StackToken reverse[1];
	stack_token_new(reverse, stack_token_capacity(stack));

	StackToken copy[1];
	stack_token_new(copy, stack_token_capacity(stack));

	while (!stack_token_is_empty(stack)) {
		stack_token_push(reverse, stack_token_pop(stack));
	}

	while (!stack_token_is_empty(reverse)) {
		stack_token_push(copy, stack_token_pop(reverse));
	}

	while (!stack_token_is_empty(copy)) {
		stack_token_push(stack, stack_token_pop(copy));
	}

	stack_token_free(reverse);
	stack_token_free(copy);
}

/// Leaves the `infix` stack empty after a successful run. So, it should be freed after this
/// function returns.
StackToken infix_to_postfix(StackToken infix) {
	StackToken postfix;
	stack_token_new(&postfix, stack_token_capacity(&infix));

	// The intermediate stack is used for storing the operators.
	StackToken stack;
	stack_token_new(&stack, stack_token_capacity(&infix));

	while (!stack_token_is_empty(&infix)) {
		const Token current = stack_token_pop(&infix);
		const TokenType type = current.type;

		if (type == TT_NUMBER) {
			Token recent = current;
			stack_token_push(&postfix, recent);
		} else if (type == TT_PARENTHESIS_LEFT) {
			stack_token_push(&stack, current);
		} else if (type == TT_PARENTHESIS_RIGHT) {
			while (!stack_token_is_empty(&stack) && stack_token_peek(&stack).type != TT_PARENTHESIS_LEFT) {
				Token recent = stack_token_pop(&stack);
				stack_token_push(&postfix, recent);
			}

			if (stack_token_is_empty(&stack)) {
				fprintf(stderr, "Unopened parenthesis\n");
				exit(-1);
			}

			stack_token_pop(&stack);
		} else if (stack_token_is_empty(&stack)) {
			stack_token_push(&stack, current);
		} else if (stack_token_peek(&stack).type == TT_PARENTHESIS_LEFT) {
			stack_token_push(&stack, current);
		} else {
			while (!stack_token_is_empty(&stack) &&
			       operator_precedence(stack_token_peek(&stack).type) > operator_precedence(type)) {
				Token recent = stack_token_pop(&stack);
				stack_token_push(&postfix, recent);
			}

			while (!stack_token_is_empty(&stack) &&
			       operator_precedence(stack_token_peek(&stack).type) == operator_precedence(type) &&
			       operator_associativity(stack_token_peek(&stack).type) == LEFT_TO_RIGHT) {
				Token recent = stack_token_pop(&stack);
				stack_token_push(&postfix, recent);
			}

			stack_token_push(&stack, current);
		}
	}

	while (!stack_token_is_empty(&stack)) {
		Token recent = stack_token_pop(&stack);
		stack_token_push(&postfix, recent);
	}

	stack_token_free(&stack);

	return postfix;
}

int main(int argc, char **argv) {
	const char *str = NULL;
	if (argc == 2) {
		str = argv[1];
	} else {
		str = "121 + 232.69 * ((65 + 4) - 42 / 5) ^ 2";
	}

	Token *tokens = lexer_analyze(str);

	StackToken infix[1];
	stack_token_new(infix, 1024);

	// TODO: keep the EOF token in the stack to make it easier to do popping out all operators in the end.
	for (int x = 0; tokens[x].type != TT_EOF; ++x) {
		stack_token_push(infix, tokens[x]);
	}

	free(tokens);

	stack_token_reverse(infix);

	StackToken postfix[1] = {infix_to_postfix(*infix)};
	stack_token_free(infix);

	printf("%s\n", str);

	Num value = evaluate_postfix(postfix);
	printf("= %lf\n", value);

	stack_token_free(postfix);

	return 0;
}

/* Usage:
#define GENERIC_STACK_ALIAS StackToken
#define GENERIC_STACK_T Token
#define GENERIC_STACK_METHOD(fn) stack_token_ ## fn
#define GENERIC_STACK_IMPLEMENTATION  // Optional
#define GENERIC_STACK_STATIC  // Optional
#include "generic_stack.h"
*/

#if !defined(GENERIC_STACK_ALIAS) || !defined(GENERIC_STACK_T) || !defined(GENERIC_STACK_METHOD)
#error "Generic arguments not defined."
#endif

#ifdef GENSTACKDEF
#undef GENSTACKDEF
#endif // GENSTACKDEF

#ifndef GENSTACKDEF
#if defined(GENERIC_STACK_STATIC)
#define GENSTACKDEF static
#elif defined(__cplusplus)
#define GENSTACKDEF extern "C"
#else
#define GENSTACKDEF extern
#endif
#endif // GENSTACKDEF

#include <stdbool.h> // bool
#include <stddef.h>  // size_t

typedef struct {
	GENERIC_STACK_T *buf;
	size_t len;
	size_t cap;
} GENERIC_STACK_ALIAS;

GENSTACKDEF void GENERIC_STACK_METHOD(new)(GENERIC_STACK_ALIAS *self, size_t cap);
GENSTACKDEF void GENERIC_STACK_METHOD(free)(GENERIC_STACK_ALIAS *self);
GENSTACKDEF bool GENERIC_STACK_METHOD(is_empty)(GENERIC_STACK_ALIAS *self);
GENSTACKDEF bool GENERIC_STACK_METHOD(is_full)(GENERIC_STACK_ALIAS *self);
GENSTACKDEF void GENERIC_STACK_METHOD(push)(GENERIC_STACK_ALIAS *self, GENERIC_STACK_T item);
GENSTACKDEF GENERIC_STACK_T GENERIC_STACK_METHOD(peek)(GENERIC_STACK_ALIAS *self);
GENSTACKDEF GENERIC_STACK_T GENERIC_STACK_METHOD(pop)(GENERIC_STACK_ALIAS *self);

GENSTACKDEF size_t GENERIC_STACK_METHOD(len)(GENERIC_STACK_ALIAS *self);
GENSTACKDEF size_t GENERIC_STACK_METHOD(capacity)(GENERIC_STACK_ALIAS *self);

#ifdef GENERIC_STACK_IMPLEMENTATION

#include <assert.h> // assert()
#include <stdlib.h> // malloc()

GENSTACKDEF void GENERIC_STACK_METHOD(new)(GENERIC_STACK_ALIAS *self, size_t cap) {
	self->buf = (GENERIC_STACK_T *) malloc(cap * sizeof(GENERIC_STACK_T));
	self->len = 0;
	self->cap = cap;
}

GENSTACKDEF void GENERIC_STACK_METHOD(free)(GENERIC_STACK_ALIAS *self) {
	if (self->buf == NULL) {
		return;
	}

	free(self->buf);
	self->buf = NULL;
	self->len = 0;
	self->cap = 0;
}

GENSTACKDEF bool GENERIC_STACK_METHOD(is_empty)(GENERIC_STACK_ALIAS *self) {
	return self->len == 0;
}

GENSTACKDEF bool GENERIC_STACK_METHOD(is_full)(GENERIC_STACK_ALIAS *self) {
	return self->len >= self->cap;
}

GENSTACKDEF void GENERIC_STACK_METHOD(push)(GENERIC_STACK_ALIAS *self, GENERIC_STACK_T item) {
	assert(!GENERIC_STACK_METHOD(is_full)(self));
	self->buf[self->len] = item;
	self->len += 1;
}

GENSTACKDEF GENERIC_STACK_T GENERIC_STACK_METHOD(peek)(GENERIC_STACK_ALIAS *self) {
	assert(!GENERIC_STACK_METHOD(is_empty)(self));
	return self->buf[self->len - 1];
}

GENSTACKDEF GENERIC_STACK_T GENERIC_STACK_METHOD(pop)(GENERIC_STACK_ALIAS *self) {
	assert(!GENERIC_STACK_METHOD(is_empty)(self));
	self->len -= 1;
	return self->buf[self->len];
}

GENSTACKDEF size_t GENERIC_STACK_METHOD(len)(GENERIC_STACK_ALIAS *self) {
	return self->len;
}

GENSTACKDEF size_t GENERIC_STACK_METHOD(capacity)(GENERIC_STACK_ALIAS *self) {
	return self->cap;
}

#undef GENERIC_STACK_IMPLEMENTATION
#endif // GENERIC_STACK_IMPLEMENTATION

#undef GENERIC_STACK_T
#undef GENERIC_STACK_ALIAS
#undef GENERIC_STACK_METHOD
